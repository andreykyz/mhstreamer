package main

import (
	"flag"
	"log"

	"gocv.io/x/gocv"
)

var stand = flag.Bool("stand", true, "Standalone with window")
var laddr = flag.String("laddr", "127.0.0.1:9000", "Listen address")
var saveFile = flag.String("out", "", "Output frames")

func main() {
	flag.Parse()
	trunk := NewTrunk(*laddr, *stand)
	log.Print("Run window")
	if *stand {
		var window *gocv.Window
		window = gocv.NewWindow("Server standalone mode")

		for {
			
			b := <-trunk.frame

			img, err := gocv.IMDecode(b, gocv.IMReadColor)
			if img.Empty() {
				log.Println("Error reading image: ", err)
				return
			}

			if *stand {
				window.IMShow(img)
				if window.WaitKey(1) >= 0 {
					break
				}
			}
		}
	}
}

package main

import (
	"flag"
	"fmt"
	"log"
	"time"

	"gocv.io/x/gocv"
)

var stand = flag.Bool("stand", false, "Standalone with window")
var deviceID = flag.Int("cam", 0, "-cam [camera ID]")
var raddr = flag.String("raddr", "127.0.0.1:9000", "Remote server")
var saveFile = flag.String("out", "", "Output frames")

func main() {
	flag.Parse()

	webcam, err := gocv.OpenVideoCapture(*deviceID)
	if err != nil {
		fmt.Printf("Error opening video capture device: %v\n", *deviceID)
		fmt.Println(err)
		return
	}
	defer webcam.Close()

	trunk := NewCameraTrunkConnect(*raddr, 0, UNLIMIT)
	img := gocv.NewMat()

	var window *gocv.Window
	if *stand {
		window = gocv.NewWindow("MHStreamer standalone version")
	}

	defer img.Close()
	frameNum := 0
	for i := 0; ; i++ {
		if ok := webcam.Read(&img); !ok {
			log.Printf("cannot read device %v\n", deviceID)
			return
		}
		if img.Empty() {
			log.Printf("no image on device %v\n", deviceID)
			return
		}
		if *saveFile != "" {
			filename := fmt.Sprintf("%06d_%s", i, *saveFile)
			log.Println(filename)
			gocv.IMWrite(filename, img)
		}

		//convert to []byte jpeg
		buf, err := gocv.IMEncodeWithParams(gocv.JPEGFileExt, img, []int{gocv.IMWriteJpegQuality, 30, gocv.IMWriteJpegOptimize, 1})
		if err == nil {
			select {
			case trunk.frame <- buf:
			default:
				log.Println("Drop frame !!! ", frameNum, " ", i)
				frameNum++
			}
		} else {
			log.Println("Error encode ", err)
		}

		if *stand {
			window.IMShow(img)
			if window.WaitKey(1) >= 0 {
				break
			}
		}
		time.Sleep(100 * time.Millisecond)
	}
}

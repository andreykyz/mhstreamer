package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"log"
	"net"
	"sync"
	"time"
)

// Connecting status
const (
	FAIL       = iota
	CONNECTING = iota
	CONNECTED  = iota
	UNLIMIT    = -1
	FRAMESIZE  = 23 * 64
)

// Magic for camera client and monitor
const (
	MAGICCAMERA  = 0xfeedf00d
	MAGICMONITOR = 0xfeedface
)

// TrunkConn is wrapper on upd connetc like conn
type TrunkConn struct {
	lAddr *net.UDPAddr
	rAddr *net.UDPAddr
	conn  *net.UDPConn
	in    chan []byte
	out   chan []byte
}

// Trunk common sctructure
type Trunk struct {
	trunkMakr byte
	status    int
	raddr     string
	tries     int
	conn      net.Conn
	tConn     *TrunkConn //for multiplexin
	myID      [32]byte   // last 4 bit is trunk marker
	frame     chan []byte
	rSeq      uint64
	wSeq      uint64
	magic     uint32
}

// NewCameraTrunkConnect connect new trunk camera client to server
func NewCameraTrunkConnect(raddr string, mark int, tries int) (trunk *Trunk) {
	trunk = &Trunk{status: CONNECTING, raddr: raddr, trunkMakr: 2, tries: tries}
	trunk.frame = make(chan []byte, 2)
	go func() {
		for trunk.tries != 0 {
			trunk.status = CONNECTING
			if trunk.tries > 0 {
				trunk.tries--
			}
			var err error
			trunk.conn, err = net.Dial("udp", trunk.raddr)
			if err != nil {
				trunk.status = FAIL
				log.Println("Dial fail ", err)
			}
			//TODO should rewrite for inti connect and worked connect
			if trunk.status == CONNECTING {
				log.Printf("Connecting...")
				// Put my magic
				bMagic := make([]byte, 4)
				binary.BigEndian.PutUint32(bMagic, MAGICCAMERA)
				// Send myId to the Server
				bTime := make([]byte, 8)
				binary.BigEndian.PutUint64(bTime, uint64(time.Now().Unix()))
				trunk.myID = sha256.Sum256(bTime)
				b := make([]byte, 4+32)

				trunk.myID[len(trunk.myID)-1] = (trunk.myID[len(trunk.myID)-1] & 0xF0) | trunk.trunkMakr

				copy(b[0:4], bMagic[:])
				copy(b[4:32+4], trunk.myID[:])
				log.Printf("Send  \n%s\n%s %d \n", hex.Dump(b), "len", len(b))
				_, err = trunk.conn.Write(b)
				if err != nil {
					log.Println("Write magic error ", err)
				}
				// Wait 5 seconds max recive myID and compare it
				trunk.conn.SetReadDeadline(time.Now().Add(time.Second * 5))
				var bRecv [32]byte
				_, err = trunk.conn.Read(bRecv[:])
				log.Printf("Recv ID  \n%s\n%s %d \n", hex.Dump(bRecv[:]), "len", len(b))

				if err == nil && (bytes.Compare(bRecv[:], trunk.myID[:]) == 0) {
					trunk.status = CONNECTED
				} else {
					log.Println("Error is ", err)
				}
			}
			if trunk.status == CONNECTED {
				readCh := make(chan []byte)
				errorCh := make(chan error)

				// Start a goroutine to read from our net connection
				go func(readCh chan []byte, errorCh chan error) {
					for {
						// try to read the data
						data := make([]byte, FRAMESIZE)
						_, err := trunk.conn.Read(data)
						if err != nil {
							// send an error if it's encountered
							errorCh <- err
							return
						}
						// send data if we read some.
						readCh <- data
					}
				}(readCh, errorCh)

				for trunk.status == CONNECTED {

					select {
					//Write
					case frame := <-trunk.frame:
						trunk.wSeq += uint64(len(frame))
						log.Print("Frame len ", len(frame), "byte")
						trunk.conn.Write(frame)
					case seqRaw := <-readCh:
						trunk.rSeq = binary.BigEndian.Uint64(seqRaw)

					}

				}
			}
			time.Sleep(1 * time.Second)
		}
	}()
	return trunk
}

//NewTrunk used for accept new connection on server
func NewTrunk(laddr string, stand bool) (trunk *Trunk) {

	udpAddr, err := net.ResolveUDPAddr("udp", laddr)
	ln, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		log.Print("Listen fail ", err)
		return
	}
	log.Println("Server stand ", udpAddr)
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			log.Print("Listen")
			b := make([]byte, 1024*1024)
			_, raddr, err := ln.ReadFromUDP(b)
			if err != nil {
				log.Print("Accept fail ", err)
				return
			}
			// if n != len(b) {
			// 	continue
			// }
			//TODO create new UDPConn instead of ln
			conn := ln

			trunk = &Trunk{conn: conn}
			trunk.tConn = &TrunkConn{lAddr: udpAddr, rAddr: raddr, conn: conn}
			copy(trunk.myID[:], b[4:4+32])

			go trunk.ServerWithCamera(b)
			println("svert stand ", stand)
			if stand {
				stand = false
				wg.Done()
			}
		}
	}()
	wg.Wait()
	return trunk
}

//ServerWithCamera run process with camera comunication
func (trunk Trunk) ServerWithCamera(b []byte) {
	log.Println("Connect server with camera")
	conn := trunk.tConn.conn
	// b := make([]byte, 8*8*8)
	// n, err := conn.Read(b)
	// if err != nil {
	// 	fmt.Println("Error reading first []byte from: ", conn, " ", err)
	// }
	n := 36
	log.Printf("Recv1  \n%s\n%s %d \n", hex.Dump(b), "len", len(b))
	trunk.magic = binary.BigEndian.Uint32(b[0:4])
	if (trunk.magic != MAGICCAMERA) && (trunk.magic != MAGICMONITOR) {
		trunk.status = FAIL
		return
	}
	// convert magic to know who connnect
	// if err != nil {
	// 	log.Print("Error reading first []byte from: ", conn, " ", err)
	// }

	//Read ID
	copy(trunk.myID[:], b[4:32+4])
	trunk.trunkMakr = trunk.myID[len(trunk.myID)-1] & 0x0F
	log.Printf("Recv2 \n%s\n %s %d \n", hex.Dump(b[0:36]), " len ", n)

	conn.WriteToUDP(trunk.myID[:], trunk.tConn.rAddr)
	trunk.status = CONNECTED
	for {
		n, _, err := conn.ReadFromUDP(b)
		if err != nil {
			log.Print("Read frame ", err," " ,n)
			return
		}
	}
}

func (conn TrunkConn) Read(b []byte) (int, error) {
	return conn.conn.Read(b)
}

func (conn TrunkConn) Write(b []byte) (int, error) {
	return conn.conn.WriteToUDP(b, conn.rAddr)
}
